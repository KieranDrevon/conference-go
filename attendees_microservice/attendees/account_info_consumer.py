from datetime import date, datetime
import json
import queue
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()


from attendees.models import AccountVO


def update_accountVO_object(ch, method, properties, body):
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = datetime.fromisoformat(updated_string)
    # updated = updated_string
    if is_active:
        AccountVO.objects.update_or_create(
            email=email,
            defaults={
                    "first_name": first_name,
                    "last_name": last_name,
                    "updated": updated,
                }
        )
    else:
        AccountVO.objects.filter(email=email).delete()


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.exchange_declare(exchange="account_info", exchange_type="fanout")
        result = channel.queue_declare(queue="", exclusive=True)
        que_name = result.method.queue
        channel.queue_bind(exchange="account_info", queue=que_name)
        channel.basic_consume(
            queue=que_name,
            on_message_callback=update_accountVO_object,
            auto_ack=True
        )
        channel.start_consuming()

    except AMQPConnectionError:
        print("Connection to RabbitMQ failed")
        time.sleep(2.0)
