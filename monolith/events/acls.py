from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_image(search_term):
    url = "https://api.pexels.com/v1/search?query=" + search_term
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    data = json.loads(response.content)
    picture_url = data["photos"]

    return picture_url[0]


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct?q=" + city + "," + state + "usa&appid=" + OPEN_WEATHER_API_KEY
    response = requests.get(url)
    data = json.loads(response.content)[0]
    lon = data["lon"]
    lat = data["lat"]
    lon = str(lon)
    lat = str(lat)

    url2 = "https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&appid=" + OPEN_WEATHER_API_KEY + "&units=imperial"
    response2 = requests.get(url2)
    weather_data = json.loads(response2.content)
    weather = {
        "temperature": weather_data["main"]["temp"],
        "description": weather_data["weather"][0]["description"],
    }
    return weather
